#!/bin/sh

runtime_directory="${HOME}/.vim"
legacy_pack_directory="${runtime_directory}/pack/legacy-plugins/start"
git_pack_directory="${runtime_directory}/pack/git-plugins/start"

mkdir -p -- "${runtime_directory}" \
            "${git_pack_directory}"

install_alternate_plugin() {
    local plugin_name="alternate"
    local install_directory="${legacy_pack_directory}/${plugin_name}"
    local plugin_directory="${install_directory}/plugin"
    local doc_directory="${install_directory}/doc"
    mkdir -p -- "${plugin_directory}" "${doc_directory}"
    cd -- "${plugin_directory}"
    curl --silent --remote-header-name --remote-name 'https://www.vim.org/scripts/download_script.php?src_id=7218'
    cd -- "${doc_directory}"
    curl --silent --remote-header-name --remote-name 'https://www.vim.org/scripts/download_script.php?src_id=6347'
}

install_bufexplorer_plugin() {
    filename="bufexplorer-7.4.21.zip"
    install_directory="${legacy_pack_directory}/${filename}"
    mkdir -p -- "${install_directory}" && cd -- "${install_directory}"
    curl --silent --output "${filename}" 'https://www.vim.org/scripts/download_script.php?src_id=26416' &&
    unzip "${filename}" &&
    rm "${filename}"
}

install_genutils_plugin() {
    filename="genutils-2.5.zip"
    install_directory="${legacy_pack_directory}/${filename}"
    mkdir -p -- "${install_directory}" && cd -- "${install_directory}"
    curl --silent --output "${filename}" 'https://www.vim.org/scripts/download_script.php?src_id=11399' &&
    unzip -o "${filename}" &&
    rm "${filename}"
}

install_lookupfile_plugin() {
    filename="lookupfile-1.8.zip"
    install_directory="${legacy_pack_directory}/${filename}"
    mkdir -p -- "${install_directory}" && cd -- "${install_directory}"
    curl --silent --output "${filename}" 'https://www.vim.org/scripts/download_script.php?src_id=11399' &&
    unzip -o "${filename}" &&
    rm "${filename}"
}

install_taglist_plugin() {
    filename="taglist-46.zip"
    install_directory="${legacy_pack_directory}/${filename}"
    mkdir -p -- "${install_directory}" && cd -- "${install_directory}"
    curl --silent --output "${filename}" 'https://www.vim.org/scripts/download_script.php?src_id=19574' &&
    unzip -o "${filename}" &&
    rm "${filename}"
}

install_enhancedcommentify_plugin() {
    filename="enhanced-commentify-2.3.tar.gz"
    cd -- "${legacy_pack_directory}"
    curl --silent --output "${filename}" 'https://www.vim.org/scripts/download_script.php?src_id=8319' &&
    tar xzf "${filename}" &&
    rm "${filename}"
}

install_ale_plugin() {
    git clone https://github.com/dense-analysis/ale.git "${git_pack_directory}/ale"
}

install_vundle_plugin() {
    git clone https://github.com/VundleVim/Vundle.vim.git "${runtime_directory}/bundle/Vundle.vim"
    {  
        printf 'set rtp+=~/.vim/bundle/Vundle.vim\n'
        printf 'so %s\n' "${runtime_directory}/vundle-plugins.vim"
    } >> "${runtime_directory}/vimrc"
}

write_vundle_install_list() {
    {
        printf 'call vundle#begin()\n'
        printf "Plugin '%s'\n" \
            'VundleVim/Vundle.vim'
        printf 'call vundle#end()\n'
    } > ${runtime_directory}/vundle-plugins.vim
}

update_vundle_plugins() {
    vim +PluginInstall +qall
}

update_help_tags() {
    vim -c "helptags ALL" -c "quitall"
}

install_alternate_plugin
install_bufexplorer_plugin
install_genutils_plugin
install_lookupfile_plugin
install_taglist_plugin
install_enhancedcommentify_plugin
install_ale_plugin
install_vundle_plugin
write_vundle_install_list
update_vundle_plugins
update_help_tags
