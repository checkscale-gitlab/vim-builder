version=$(git tag | sort -V | tail -1)

./configure \
    --with-compiledby="Jean-Rene David <jeanrene.david@gmail.com>" \
    --with-features=huge \
    --enable-cscope \
    --with-x \
    --enable-pythoninterp=yes \
    --enable-python3interp=yes \
    --prefix="/usr/local/stow/vim" &&
make && make install
